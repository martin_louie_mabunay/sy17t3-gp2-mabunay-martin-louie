﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject1
{
    class Program
    {
        public struct Item
        {
            public string name;
            public int gold;

            public Item(string name, int gold)
            {
                this.name = name;
                this.gold = gold;
            }
        };

        public static Item generateItem(string name, int gold)
        {
            Item item;
            item.name = name;
            item.gold = gold;

            return item;
        }

        static void pickUp(ref int bank, int multiplier,Item item)
        {
            bank += item.gold * multiplier;
            Console.WriteLine("You picked up: " + item.name);
            Console.WriteLine("You received: " + item.gold * multiplier + " gold");
        }

        static void enterDungeon(ref int gold, int bank, int multiplier)
        {
            // Item loot = GenerateItem();
            // if cursed stone
            // if not cursed stone

            //Not sure which is more efficient, the function or the struct constructor
            Item item1 = generateItem("Mitrhil Ore", 100);
            Item item2 = generateItem("Sharp Talon", 50);
            Item item3 = generateItem("Thick Leather", 25);
            Item item4 = generateItem("Jellopy", 5);
            Item item5 = generateItem("Cursed Stone", 0);
          
            char input;
            int outsideGold;

            Console.WriteLine();
            Console.WriteLine();

            for (;;)
            {
                //Generates random number to pick up a random item
                Random random = new Random();
                int rng = random.Next(1, 6);

                Console.Clear();

                Console.WriteLine("Gold multiplier: " + multiplier);
                
                //Picks up Mithril Ore
                if (rng == 1)
                {
                    pickUp(ref bank, multiplier, item1);
                }
                //Picks up Sharp Talon
                else if (rng == 2)
                {
                    pickUp(ref bank, multiplier, item2);
                }
                //Picks up Thick Leather
                else if (rng == 3)
                {
                    pickUp(ref bank, multiplier, item3);
                }
                //Picks up Jellopy
                else if (rng == 4)
                {
                    pickUp(ref bank, multiplier, item4);
                }
                //Picks up Cursed Stone
                else if (rng == 5)
                {
                    pickUp(ref bank, multiplier, item5);
                    Console.WriteLine("You lose");
                    Console.ReadLine();
                    break;
                }

                Console.WriteLine("Bank: " + bank);
                Console.WriteLine();

                Console.WriteLine("Do you want to continue looting? [Y][N]");
                input = Console.ReadKey().KeyChar;

                Console.WriteLine();
                Console.WriteLine();

                //Adds multiplier by 1 
                if (input == 'Y') multiplier++;

                //Upon exit, puts all gold gained from the dungeon to player's gold
                else if (input == 'N')
                {
                    outsideGold = gold;
                    gold = bank + outsideGold;
                    bank = 0;
                    return;
                }
            }
        }

        static void Main(string[] args)
        {
            int gold = 50;
            int bank = 0;
            int multiplier = 1;
            int entryCost = 25;
            char input;


            while (true)
            {
                Console.WriteLine("Your gold: " + gold);
                Console.WriteLine("Dungeon entry cost is " + entryCost);

                //Win conditions
                if (gold >= 500)
                {
                    Console.WriteLine("You win");
                    break;
                }

                Console.Write("Do you want to enter? [Y][N]");
                input = Console.ReadKey().KeyChar;

                //Cannot enter dungeon with gold less than the entryCost
                if (gold <= 26)
                {
                    Console.WriteLine("You don't have enough gold to enter");
                    Console.WriteLine("You lose");
                    break;
                }

                if (input == 'Y')
                {
                    gold -= entryCost;
                    enterDungeon(ref gold, bank, multiplier);
                    
                }
                else if (input == 'N') break;
            }

            Console.WriteLine("Bye");
            Console.WriteLine();

            input = Console.ReadKey().KeyChar;
        }
    }
}
