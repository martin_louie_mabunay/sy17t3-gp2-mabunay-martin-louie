﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    AudioSource BackgroundMusic;
    // Use this for initialization
    void Start ()
    {
        BackgroundMusic = GetComponent<AudioSource>();
        BackgroundMusic.Play();
    }
	
	// Update is called once per frame
	void Update ()
    {
        BackgroundMusic.loop = true;
    }
}
