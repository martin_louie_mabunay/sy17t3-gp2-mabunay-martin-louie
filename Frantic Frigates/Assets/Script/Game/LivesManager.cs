﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesManager : MonoBehaviour
{
    public static int lives;
    public GameObject player;

    Text text;

    // Use this for initialization
    void Start ()
    {
        text = GetComponent<Text>();
        lives = player.GetComponent<PlayerStats>().lives;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (player == null) {}
        else
        {
            lives = player.GetComponent<PlayerStats>().lives;
            text.text = "Lives: " + lives;
        }
	}
}
