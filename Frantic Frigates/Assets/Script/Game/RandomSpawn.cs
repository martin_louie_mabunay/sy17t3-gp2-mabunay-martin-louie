﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    public List<GameObject> Enemy;

    public float spawnRate;
    private float lastSpawn= 0.0f;

    int counter;
    int randomEnemy;

    public GameObject player;
    GameObject spawn;
    GameObject clone;
    List<GameObject> gList = new List<GameObject>();

    


    void Awake()
    {
        clone = Enemy[1];
    }

    // Use this for initialization
    void Start ()
    {
        for(int i = 0; i < Enemy.Count; i++)
        {
            gList.Add(Enemy[i]);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log(gList.Count);
        SpawnObjects();
	}

    void SpawnObjects()
    {
        if (Time.time > spawnRate + lastSpawn)
     	{
            counter = Random.Range(1, 4);
            randomEnemy = Random.Range(0, gList.Count);

            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2 (0,0));
            Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2 (1,1));

            GameObject spawn = gList[randomEnemy];
            
            if (spawn == null) spawn = GameObject.Find("Enemy Ship(Clone)");

            if (counter == 1)
            {
                spawn.transform.position = new Vector2(Random.Range(min.x, max.x), min.y);
            }
            else if (counter == 2)
            {
                spawn.transform.position = new Vector2(min.x, Random.Range(min.y, max.y));
            }

            else if(counter == 3)
            {   
                spawn.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
            }

            else if(counter == 4)
            {
                spawn.transform.position = new Vector2(max.x, Random.Range(min.y, max.y));
            }         

            GameObject enemySpawn = (GameObject)Instantiate (spawn);

            if (spawn == Enemy[0])
            {              
                Vector2 direction = player.transform.position - enemySpawn.transform.position;
                enemySpawn.GetComponent<Follow>().SetDirection(direction);
            }

            GameObject clone = Enemy[1];

            Debug.Log(randomEnemy);

			lastSpawn = Time.time;	
           
		}

    }
}
