﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    Text text;

    void Awake()
    {
        
    }

    // Use this for initialization
    void Start ()
    {       
        text = GetComponent<Text>();
        text.text = "";
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (LivesManager.lives <= 0) text.text = "GAMEOVER";
    }
}
