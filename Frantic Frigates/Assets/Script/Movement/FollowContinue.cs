﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FollowContinue : MonoBehaviour 
{
	public GameObject target;
	public float moveSpeed;


	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//if (gameObject == null) throw new Exception();
		if (target == null) {}
		else 
		{
			transform.position = Vector3.MoveTowards(transform.position, target.transform.position, moveSpeed);
			faceDirection();

			Vector3 pos = transform.position; 
	
			pos.x = Mathf.Clamp (pos.x, -1, 1);
		}
	}

	void faceDirection()
	{
		Vector3 targetPos = target.transform.position;

		Vector2 direction = new Vector2(
			targetPos.x - transform.position.x,
			targetPos.y - transform.position.y
		);

		transform.up = direction;
	}
}
