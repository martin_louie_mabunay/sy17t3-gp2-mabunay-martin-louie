﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    //Vector3 direction;
    private Vector2 _direction;
    public float speed;

    void Awake()
    {
        faceDirection();
    }

    // Use this for initialization
    void Start ()
    {
       
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 position = transform.position;

        position += _direction * speed * Time.deltaTime;

        transform.position = position;

        
    }

    public void SetDirection(Vector2 direction)
    {
        _direction = direction.normalized;
    }

    void faceDirection()
	{

		Vector2 direction = new Vector2(
			_direction.x - transform.position.x,
			_direction.y - transform.position.y
		);

		transform.right = direction;
	}

    
}
