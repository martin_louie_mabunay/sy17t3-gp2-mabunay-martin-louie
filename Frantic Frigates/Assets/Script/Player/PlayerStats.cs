﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour 
{
    public int lives;
    private bool invincible;

    // Use this for initialization
    void Start ()
	{
        invincible = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
        // if (invincible) Debug.Log("Invincible is TRUE");
        // if (!invincible) Debug.Log("Invincible is FALSE");
	}

    private IEnumerator OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player Bullet") { }  
        else
        {
            if (!invincible)
            {
                if (collision.tag == "Enemy") lives -= 1;
                else if (collision.tag == "Enemy Bullet") lives -= 1; 
                invincible = true;
            }
            if (invincible)
            {
                yield return new WaitForSeconds(3);
                invincible = false;
            }
        }

        if (lives <= 0)
        {
            Destroy(gameObject);
            Time.timeScale = 0;
        }
    }
}
