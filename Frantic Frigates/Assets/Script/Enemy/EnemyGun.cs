﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour 
{
	public GameObject Bullet;
	private float lastShot = 0.0f;
	public float fireRate;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		FireBullet();
	}
	
	void FireBullet()
	{
		if (Time.time > fireRate + lastShot)
     	{
			GameObject enemy = null;
			enemy = FindClosestEnemy();

			GameObject _bullet = Instantiate (Bullet) as GameObject;

			_bullet.transform.position = transform.position;

			Vector2 direction = enemy.transform.position - _bullet.transform.position;

			_bullet.GetComponent<EnemyBullet>().SetDirection(direction);	

			lastShot = Time.time;	
		}
	}

	public GameObject FindClosestEnemy()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Player");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }
}
