﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour 
{
    public float speed;
    Vector2 _direction;
    public int damage;

    // Use this for initialization
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 position = transform.position;

        position += _direction * speed * Time.deltaTime;

        transform.position = position;
    }

    public void SetDirection(Vector2 direction)
    {
        _direction = direction.normalized;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") Destroy(gameObject);
    }

    public float Damage()
    {
        return damage;
    }
}
