﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour 
{
	public int health;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	private void OnTriggerEnter2D(Collider2D collision)
    {              
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
        }
        else if (collision.tag == "Player Bullet")
        {
            health -= collision.GetComponent<PlayerBullet>().damage;
        }
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
