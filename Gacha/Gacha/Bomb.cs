﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    public class Bomb : Item
    {
        public Bomb() { Name = "Bomb"; }
        public override void Activate(Player player)
        {
            player.HP -= Value;
            Console.WriteLine("You pulled Bomb ");
            Console.WriteLine("You received: " + Value + " damage");
        }
    }
}
