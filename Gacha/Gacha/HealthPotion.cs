﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    public class HealthPotion : Item
    {
        public HealthPotion() { Name = "Health Potion"; }
        public override void Activate(Player player)
        {
            player.HP += Value;
            Console.WriteLine("You pulled Health Potion ");
            Console.WriteLine("You received: " + Value + " Health Points");
        }
    }
}
