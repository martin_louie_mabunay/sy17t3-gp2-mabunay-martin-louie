﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    public class SSR : Item
    {
        public SSR() { Name = "SSR"; }

        public override void Activate(Player player)
        {
            player.RarityPoints += Value;
            Console.WriteLine("You pulled SSR ");
            Console.WriteLine("You received: " + Value + " Rarity Points");
        }
    }
}
