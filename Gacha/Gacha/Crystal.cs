﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    public class Crystal : Item
    {
        public Crystal() { Name = "Crystal"; }
        public override void Activate(Player player)
        {
            player.Crystals += Value;
            Console.WriteLine("You pulled Crystals ");
            Console.WriteLine("You received: " + Value + " Crystals");
        }
    }
}
