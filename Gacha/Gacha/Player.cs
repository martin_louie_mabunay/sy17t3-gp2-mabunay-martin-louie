﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    public class Player
    {
        public int Crystals { get; set; }
        public int HP { get; set; }
        public int RarityPoints { get; set; }
        public int Pulls { get; set; }

        public List<Item> Gacha { get; set; }
        public List<Item> ItemPool { get; set; }

        public Player() { Gacha = new List<Item>(); }

        public void DisplayStats()
        {
            Console.WriteLine("HP: " + HP);
            Console.WriteLine("Crystals: " + Crystals);
            Console.WriteLine("Rarity Points: " + RarityPoints);
            Console.WriteLine("Pulls: " + Pulls);
        }
    }
}
