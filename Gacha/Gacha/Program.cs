﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    class Program
    {
        static Random random = new Random();
        static void Main(string[] args)
        {
            // Initializers
            #region 
            // Player
            Player player = new Player();
            player.HP = 100;
            player.Crystals = 100;
            player.RarityPoints = 0;

            // Items
            Item item = new Item();

            // Rarity Points
            SSR ssr = new SSR { Value = 50 };
            player.Gacha.Add(ssr);
            SR sr = new SR { Value = 10 };
            player.Gacha.Add(sr);
            R r = new R { Value = 1 };
            player.Gacha.Add(r);

            // Health Potion
            HealthPotion healthPotion = new HealthPotion { Value = 30 };
            player.Gacha.Add(healthPotion);

            // Bomb
            Bomb bomb = new Bomb { Value = 25 };
            player.Gacha.Add(bomb);

            // Crystal
            Crystal crystal = new Crystal { Value = 15 };
            player.Gacha.Add(crystal);
            #endregion

            // Plays Gacha
            Game(item, player);
            endGame(ssr, sr, r, healthPotion, bomb, crystal, player);

            Console.ReadLine();
        }

        // Prints Game Results
        static void endGame(SSR ssr, SR sr,
                         R r, HealthPotion healthPotion,
                         Bomb bomb, Crystal crystal, Player player)
        {
            player.DisplayStats();
            Console.WriteLine();

            //player.ItemPool.GroupBy(count => count.Activate);

            foreach(Item item in player.ItemPool)
            {
                if (item.Name == "SSR") 
            }

            Console.WriteLine("Items Pulled: ");
            Console.WriteLine("SSR x" + player.ItemPool.GroupBy(item => item.GetType()));
            Console.WriteLine("SR x" + sr.Count);
            Console.WriteLine("R x" + r.Count);
            Console.WriteLine("Health Potion x" + healthPotion.Count);
            Console.WriteLine("Bomb x" + bomb.Count);
            Console.WriteLine("Crystal x" + crystal.Count);

            // Prints all the items pulled
            Console.WriteLine("Items Pulled: ");
            Console.WriteLine("SSR x" + ssr.Count);
            Console.WriteLine("SR x" + sr.Count);
            Console.WriteLine("R x" + r.Count);
            Console.WriteLine("Health Potion x" + healthPotion.Count);
            Console.WriteLine("Bomb x" + bomb.Count);
            Console.WriteLine("Crystal x" + crystal.Count);

            //player.ItemPool.GroupBy
        }


        // Main Gacha Game
        static void Game(Item item, Player player)
        {
            // Initializers
            int pullCosts = 5;
            player.Pulls = 0;
            Item Gacha = null;
            int chance = 0;

            for(;;)
            {
                // Decreases crystals per pull
                player.Crystals -= pullCosts;

                // Increases number of pulls
                player.Pulls++;

                // Probability item pull
                chance = random.Next(1, 101);

                // Pulls a Random Gacha
                gachaPull(chance, ref player, ref Gacha);
                
                // Gives player the Gacha's effects
                Gacha.Activate(player);

                // Prints player's information
                player.DisplayStats();
                Console.WriteLine();

                // Pauses
                Console.ReadLine();

                // Clears
                Console.Clear();

                // Checks Win and Lose Conditions
                if (victoryConditions(player) == false) return;
            }
        }

        // Chance to pull from gacha pool
        static void gachaPull(int chance, ref Player player,ref Item Gacha)
        {
            // Pull SSR 1% chance
            if (chance == 1)
            {
                Gacha = player.Gacha[0];
                player.ItemPool.Add(player.Gacha[0]);
            }

            // Pull SR 9% chance
            else if (chance >= 2 && chance <= 10)
            {
                Gacha = player.Gacha[1];
                player.ItemPool.Add(player.Gacha[1]);
            }

            // Pull R 40% chance
            else if (chance >= 11 && chance <= 50)
            {
                Gacha = player.Gacha[2];
                player.ItemPool.Add(player.Gacha[2]);
            }
            // Pull Health Potion 15% chance
            else if (chance >= 51 && chance <= 65)
            {
                Gacha = player.Gacha[3];
                player.ItemPool.Add(player.Gacha[3]);
            }

            // Pull Bomb 20% chance
            else if (chance >= 66 && chance <= 85)
            {
                Gacha = player.Gacha[4];
                player.ItemPool.Add(player.Gacha[4]);
            }

            // Pull Crystal 15% chance
            else if (chance >= 86 && chance <= 100)
            {
                Gacha = player.Gacha[5];
                player.ItemPool.Add(player.Gacha[5]);
            }
        }

        // Victory Conditions
        static bool victoryConditions(Player player)
        {
            if (player.Crystals <= 0)
            {
                Console.WriteLine("You ran out of crystals! ");
                Console.ReadKey();
                Console.WriteLine("You Lose! ");
                Console.WriteLine();
                return false;
            }
            else if (player.HP <= 0)
            {
                Console.WriteLine("You ran out of HP! ");
                Console.ReadKey();
                Console.WriteLine("You Lose! ");
                Console.WriteLine();
                return false;
            }
            else if (player.RarityPoints >= 100)
            {
                Console.WriteLine("You achieved 100 Rarity Points! ");
                Console.ReadKey();
                Console.WriteLine("You Win! ");
                Console.WriteLine();
                return false;
            }
            else return true;
        }

        //static int GetItems(Player player, Item item)
        //{
        //    int items = player.ItemPool.FindAll(item);
        //    return items;
        //}

    }
}
