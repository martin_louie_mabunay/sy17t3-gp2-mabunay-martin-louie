﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    public class SR : Item
    {
        public SR() { Name = "SR"; }
        public override void Activate(Player player)
        {
            player.RarityPoints += Value;
            Console.WriteLine("You pulled SR ");
            Console.WriteLine("You received: " + Value + " Rarity Points");
        }
    }
}
