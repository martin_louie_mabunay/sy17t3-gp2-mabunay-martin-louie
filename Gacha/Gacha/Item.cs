﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    public class Item
    {
        public int Value { get; set; }
        public string Name { get; set; }

        public virtual void Activate(Player player)
        {

        }
    }
}
