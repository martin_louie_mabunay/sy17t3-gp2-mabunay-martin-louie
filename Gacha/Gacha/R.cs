﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP2
{
    public class R : Item
    {
        public R() { Name = "R"; }

        public override void Activate(Player player)
        {
            player.RarityPoints += Value;
            Count++;
            Console.WriteLine("You pulled R ");
            Console.WriteLine("You received: " + Value + " Rarity Points");
        }
    }
}
