﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    public class RandomHelper
    {
        private static Random rand = new Random();
        private static readonly int CHANCE_PRECISION = Int32.MaxValue;

        public static int Range(int max)
        {
            return rand.Next(max);
        }

        public static int Range(int min, int max)
        {
            return rand.Next(min, max);
        }

        public static bool Chance(float chance)
        {
            float roll = (float)Range(0, CHANCE_PRECISION) / CHANCE_PRECISION;
            return roll < chance;
        }
    }
}
