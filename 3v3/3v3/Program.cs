﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3v3
{
    class Program
    {
        static Random random = new Random();
        static void Main(string[] args)
        {
            List<Unit> playerTeam = new List<Unit>();
            List<Unit> enemyTeam = new List<Unit>();
            List<Unit> Units = new List<Unit>();

            PlayerTeam(playerTeam);
            EnemyTeam(enemyTeam);
            TurnOrder(playerTeam, enemyTeam, Units);

            //Loops till no one is left on a team
            while (playerTeam.Count > 0 && enemyTeam.Count > 0)
            {
                Battle(playerTeam, enemyTeam, Units);
            }

            if (playerTeam.Count > 0) Console.WriteLine("Team 1 Wins!");
            else if (enemyTeam.Count > 0) Console.WriteLine("Team 2 Wins!");            
            
            Console.ReadLine();
        }
        static void Battle(List<Unit> playerTeam, List<Unit> enemyTeam, List<Unit> Units)
        {
            Skill skill = null;

            int playerChoice = 0;
            int enemyChoice = random.Next(1,3);
            PrintUnits(playerTeam, enemyTeam);
            PrintOrder(Units);

            if (Units[0].stats.Side == "Team 1")
            {
                while (playerChoice != 1 && playerChoice != 2)
                {                   
                    AskPlayer(Units[0], Units);
                    playerChoice = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine();
                    Console.Clear();
                }
                //Allies will be displayed as targets if the Wizard uses a skill
                if (Units[0].stats.Class == "Wizard" && playerChoice == 2) PrintAllies(playerTeam);
                //Otherwise enemies will be displayed as targets
                else PrintTargets(playerTeam, enemyTeam, Units);

                if (playerChoice == 1) skill = Units[0].Skill[0];
                else if (playerChoice == 2) skill = Units[0].Skill[1];

                //Checks if the unit can cast the skill
                while (skill.MpCost > Units[0].CurrentMp)
                {
                    Console.Clear();
                    AskPlayer(Units[0], Units);
                    Console.WriteLine();
                    Console.WriteLine("Not enough MP!");
                    Console.Write("Action: ");
                    playerChoice = Convert.ToInt32(Console.ReadLine());
                    if (playerChoice == 1)
                    {
                        PrintTargets(playerTeam, enemyTeam, Units);
                        skill = Units[0].Skill[0];
                        break;
                    }
                }

                    //Targets enemy team and activates the chosen skill
                    skill.activateSkill(Units[0], enemyTeam, playerTeam);
            }
            else
            {
                if (enemyChoice == 1) skill = Units[0].Skill[0];
                else if (enemyChoice == 2) skill = Units[0].Skill[1];

                //Casts basic attack if the unit doesn't have enough Mp
                if (skill.MpCost > Units[0].CurrentMp)
                {
                    skill = Units[0].Skill[0];
                }

                //Targets player  team and activates the random chosen skill
                skill.activateSkill(Units[0], playerTeam, enemyTeam);
            }
            NextTurn(Units);

        }
        static void PlayerTeam(List<Unit> playerTeam)
        {
            // Basic Attack
            BasicAttack basicAttack = new BasicAttack();
            // Shockwave
            Shockwave shockWave = new Shockwave();
            // Heal
            Heal heal = new Heal();
            // Assassinate
            Assassinate assassinate = new Assassinate();

            // Warrior
            Warrior playerWarrior = new Warrior();
            playerWarrior.Initialize("Ally Warrior", "Team 1");
            playerTeam.Add(playerWarrior);
            // Warrior Skills
            playerWarrior.Skill.Add(basicAttack);
            playerWarrior.Skill.Add(shockWave);

            // Wizard
            Mage playerMage = new Mage();
            playerMage.Initialize("Ally Wizard", "Team 1");
            playerTeam.Add(playerMage);
            // Wizard Skills
            playerMage.Skill.Add(basicAttack);
            playerMage.Skill.Add(heal);

            // Assassin
            Assassin playerAssassin = new Assassin();
            playerAssassin.Initialize("Ally Assassin", "Team 1");
            playerTeam.Add(playerAssassin);
            // Assassin Skills
            playerAssassin.Skill.Add(basicAttack);
            playerAssassin.Skill.Add(assassinate);

        }

        // Initializes Enemy Team
        static void EnemyTeam(List<Unit> enemyTeam)
        {
            //Basic Attack
            BasicAttack basicAttack = new BasicAttack();
            //Shockwave
            Shockwave shockWave = new Shockwave();
            //Heal
            Heal heal = new Heal();
            //Assassinate
            Assassinate assassinate = new Assassinate();

            //Warrior
            Warrior enemyWarrior = new Warrior();
            enemyWarrior.Initialize("Enemy Warrior", "Team 2");
            enemyTeam.Add(enemyWarrior);
            //Warrior Skills
            enemyWarrior.Skill.Add(basicAttack);
            enemyWarrior.Skill.Add(shockWave);

            //Wizard
            Mage enemyMage = new Mage();
            enemyMage.Initialize("Enemy Wizard", "Team 2");
            enemyTeam.Add(enemyMage);
            //Wizard Skills
            enemyMage.Skill.Add(basicAttack);
            enemyMage.Skill.Add(heal);

            //Assassin
            Assassin enemyAssassin = new Assassin();
            enemyAssassin.Initialize("Enemy Assassin", "Team 2");
            enemyTeam.Add(enemyAssassin);
            //Assassin Skills
            enemyAssassin.Skill.Add(basicAttack);
            enemyAssassin.Skill.Add(assassinate);
        }

        // Prints all Units
        static void PrintUnits(List<Unit> playerTeam, List<Unit> enemyTeam)
        {
            //Prints Team 1
            Console.WriteLine("Team 1 ");
            Console.WriteLine("=========================================");
            foreach(Unit unit in playerTeam) Console.WriteLine(unit.stats.Name + " [HP: " + unit.CurrentHp + "]");
            Console.WriteLine("=========================================");
            Console.WriteLine();

            //Prints Team 2
            Console.WriteLine("Team 2 ");
            Console.WriteLine("=========================================");
            foreach (Unit unit in enemyTeam) Console.WriteLine(unit.stats.Name + " [HP: " + unit.CurrentHp + "]");
            Console.WriteLine("=========================================");
            Console.WriteLine();
        }        

        static void PrintAllies(List<Unit> playerTeam)
        {
            int i = 1;
            Console.WriteLine("=========================================");
            foreach (Unit unit in playerTeam)
            {
                 Console.WriteLine("[" + i + "]" + unit.stats.Name + " [HP: " + unit.CurrentHp + "]");
                 i++;
            }
            Console.WriteLine("=========================================");           
        }

        static void TurnOrder(List<Unit> playerTeam, List<Unit> enemyTeam, List<Unit> Units)
        {
            Unit temp;
            foreach (Unit unit in playerTeam) Units.Add(unit);
            foreach (Unit unit in enemyTeam) Units.Add(unit);
            for (int i = 0; i < Units.Count; i++)
            {
                for (int j = i + 1; j < Units.Count; j++)
                {
                    deleteDead(Units);
                    if (Units[j].stats.Agi > Units[i].stats.Agi)
                    {
                        temp = Units[i];
                        Units[i] = Units[j];
                        Units[j] = temp;
                    }
                }
            }
        }

        static void deleteDead(List<Unit> Units)
        {
            for (int i = Units.Count - 1; i >= 0; i--)
            {
                if (Units[i].Alive == false)
                {
                    Units.RemoveAt(i);
                }
            }
        }

        static void PrintOrder(List<Unit> Units)
        {
            int currentTurn = 1;

            deleteDead(Units);
            Console.WriteLine("======== TURN ORDER ========");
            foreach (Unit unit in Units)
            {
                //Prints the side and name
                if (unit.Alive == true) Console.WriteLine("#" + currentTurn + " [" + unit.stats.Side + "] " + unit.stats.Name);
                currentTurn++;
            }
            Console.WriteLine("======== TURN ORDER ========");
            Console.WriteLine("Current Turn: " + Units[0].stats.Name);         
            Console.ReadLine();
            Console.Clear();
        }
        static void PrintTargets(List<Unit> playerTeam, List<Unit> enemyTeam, List<Unit> Units)
        {
            int i = 1;
            Console.WriteLine("=========================================");
            foreach (Unit unit in enemyTeam)
            {
                Console.WriteLine("[" + i + "]" + unit.stats.Name + " [HP: " + unit.CurrentHp + "]");
                i++;
            }
            Console.WriteLine("=========================================");           
        }
        static void AskPlayer(Unit player, List<Unit> Units)
        {          
            player.DisplayStats();
            Console.WriteLine();
            Console.WriteLine("Choose an Action... ");
            Console.WriteLine("=========================");
            Console.WriteLine("[1] " + Units[0].Skill[0].Name);
            Console.WriteLine("[2] "+ Units[0].Skill[1].Name);
            Console.WriteLine("=========================");
            Console.Write("Action: ");
        }

        static void NextTurn(List<Unit> Units)
        {
            int i = 0;
            Unit temp;

            for (int j =  i + 1; j < Units.Count; j++)
            {
                temp = Units[i];
                Units[i] = Units[j];
                Units[j] = temp;
                i++;
            }
        }

    }
    
}
