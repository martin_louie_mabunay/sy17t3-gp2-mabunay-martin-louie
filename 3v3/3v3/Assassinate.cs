using System;
using System.Collections.Generic;

namespace _3v3
{
    public class Assassinate : Skill
    {
        const float ASSASSINATE_DAMAGE = 2.2f;

        // Sets the Name of the skill and MP Cost
        public Assassinate()
        {
            Name = "Assassinate";
            MpCost = 40;
        }

        // Activates Skill
        public override void activateSkill(Unit actor, List<Unit> target, List<Unit> allies)
        {
            int chosenTarget = 10;
            double lowest = Double.MaxValue;
            int index = -1;
            int damage = actor.Damage(target[0], ASSASSINATE_DAMAGE);
            
            Console.Clear(); 
            
            // Finds target with lowest HP
            foreach(Unit unit in target)
            {
                index++;
                if(unit.CurrentHp <= lowest)
                {
                    lowest = unit.CurrentHp;
                    chosenTarget = index;
                }
            }            

            Console.WriteLine(actor.stats.Name + " used " + Name + " against " + target[chosenTarget].stats.Name + ".");
            Console.WriteLine(actor.stats.Name + " dealt " + damage + " damage. ");

            // Targeted enemy will receive damage
            target[chosenTarget].TakeDamage(damage);

            // Prints the targeted enemy died if it died
            if (target[chosenTarget].CurrentHp == 0) Console.WriteLine(target[chosenTarget].stats.Name + " died. ");

            // Delete the dead unit from the list
            if (target[chosenTarget].Alive == false) target.Remove(target[chosenTarget]);

            // Deducts Mana based on MpCost
            actor.ReduceMp(MpCost);

            Console.WriteLine();
            Console.ReadLine();
            Console.Clear();    
        }
    }
}
