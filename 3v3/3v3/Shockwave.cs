using System;
using System.Collections.Generic;

namespace _3v3
{
    public class Shockwave : Skill
    {
        const float SHOCKWAVE_DAMAGE = 0.9f;

        // Sets the Name of the skill and MP Cost
        public Shockwave()
        {
            Name = "Shockwave";
            MpCost = 50;
        }

        // Activates Skill
        public override void activateSkill(Unit actor, List<Unit> target, List<Unit> allies)
        {
            // Can't Cast skill if MP is insufficient
            if (actor.CurrentMp < MpCost)
            {
                Console.WriteLine(actor.stats.Name + " doesn't have enough MP");
                Console.Clear();
                Console.ReadLine();
                return;
            }

            Console.WriteLine(actor.stats.Name + " is casting " + Name + " against all opponents ");

            // Will not target dead targets
            if (actor.Alive != true) Console.WriteLine(actor.stats.Name + " is dead ");

            // Gives damage to all alive targets
            else if (actor.Alive == true)
            {
                // Different damage values targets
                foreach (Unit unit in target)
                {
                    int damage = actor.Damage(unit, SHOCKWAVE_DAMAGE);
                    Console.WriteLine(Name + " dealt " + damage + " against " + unit.stats.Name);
                    unit.TakeDamage(damage);                    
                }

                // Checks targets who died
                foreach (Unit unit in target)
                {
                    if (unit.CurrentHp == 0) Console.WriteLine(unit.stats.Name + " died ");
                }

                // Delete dead targets from the list
                for (int i = target.Count - 1; i >= 0; i--)       
                {
                    if (target[i].Alive == false) target.RemoveAt(i);
                }

                // Deducts Mana based on MpCost
                actor.ReduceMp(MpCost);
            }
            Console.WriteLine();
            Console.ReadLine();
            Console.Clear();
        }
    }
}
