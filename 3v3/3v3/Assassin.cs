using System;

namespace _3v3
{
    class Assassin : Unit
    {
        static Random random = new Random();
        public void Initialize(string name, string side)
        {          
            stats.Name = name;
            stats.MaxHp = random.Next(300, 351);
            stats.MaxMp = random.Next(110, 150);

            stats.Pow = random.Next(18, 39);
            stats.Vit = random.Next(8, 11);
            stats.Agi = random.Next(19, 35);
            stats.Dex = random.Next(20, 31);

            CurrentMp = stats.MaxMp;
            CurrentHp = stats.MaxHp;

            stats.Class = "Assassin";
            stats.Side = side;
        }
    }
}
