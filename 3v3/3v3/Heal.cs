using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3v3
{
    public class Heal : Skill
    {
        static Random random = new Random();

        const float HEAL_MODIFIER = 0.3f;

        // Sets the Name of the skill and MP Cost
        public Heal()
        {
            Name = "Heal";
            MpCost = 40;
        }

        // Activates Skill
        public override void activateSkill(Unit actor, List<Unit> target, List<Unit> allies)
        {
            int chosenTarget = 10;

             // Can't Cast skill if MP is insufficient
            if (actor.CurrentMp < MpCost)
            {
                Console.WriteLine(actor.stats.Name + " doesn't have enough MP");
                Console.Clear();
                Console.ReadLine();
                return;
            }

            // Manual targeting for player side
            if (actor.stats.Name == "Team 1")
            {
                // Loops till a valid target has been chosen
                while (chosenTarget > target.Count - 1)
                {
                    // Player will choose a target
                    Console.Write("Choose a target: ");
                    chosenTarget = Convert.ToInt32(Console.ReadLine()) - 1;
                }
            }
            // Enemy picks a target at random
            else chosenTarget = random.Next(target.Count);

            if (actor.Alive == false) Console.WriteLine(actor.stats.Name + " is dead ");
            else if (actor. Alive == true)
            {
                int healAmount = (int)(allies[0].stats.MaxHp * HEAL_MODIFIER);

                // Actor targets itself
                if (allies[chosenTarget].stats.Name == actor.stats.Name) Console.WriteLine(actor.stats.Name + " used " + Name + " on theirself ");
                // Actor
                else Console.WriteLine(actor.stats.Name + " used " + Name + " on " + allies[chosenTarget].stats.Name);
               
                // Prints healed amount to target
                Console.WriteLine(allies[chosenTarget].stats.Name + " was healed for " + healAmount + " HP");
                
                // Heals chosen target
                allies[chosenTarget].Heal(healAmount);
                
                // Deducts Mana based on MpCost
                actor.ReduceMp(MpCost);
            }
            Console.WriteLine();
            Console.ReadLine();
            Console.Clear();
        }
    }
}
