using System;
using System.Collections.Generic;

namespace _3v3
{
    public class Unit 
    {
        Random random = new Random();

        public const float CLASS_ADVANTAGE_DAMAGE = 1.5f;
        public const float RANDOM_POW_DAMAGE = 0.2f;
        public const int MAX_HIT_RATE = 80;
        public const int MIN_HIT_RATE = 20;

        Skill basicAttack = new Skill();
        Skill skill = new Skill();

        public struct Stats
        {
            public string Name;
            public string Class;
            public string Side;
            public int MaxHp;
            public int MaxMp;
            public int Pow;
            public int Vit;
            public int Agi;
            public int Dex;
        };

        public Stats stats;
 
        // Current HP of a Unit
        private int currentHp;
        public int CurrentHp
        {
            get { return currentHp; }
            set
            {
                // Clamps HP
                currentHp = value;
                if (currentHp < 0) currentHp = 0;
                if (currentHp > stats.MaxHp) currentHp = stats.MaxHp;
            }            
        }

        // Current MP of a Unit
        private int currentMp;
        public int CurrentMp
        {
            get { return currentMp; }
            set
            {
                // Clamps MP
                currentMp = value;
                if (currentMp < 0) currentMp = 0;
                if (currentMp > stats.MaxHp) currentMp = stats.MaxMp;
            }
        }
        
        public void Heal(int amount)
        {
            currentHp += amount;
            if (currentHp > stats.MaxHp) currentHp = stats.MaxHp;
        }

        // Checks if Unit is alive
        public bool Alive
        {
            get
            {
                if (CurrentHp <= 0) return false;
                else return true;
            }
        }

        // Checks if Skill will hit the target
        public bool WillHit(Unit target)
        {
            int hitPercent = random.Next(1, 101);
            // if hitPercent is greater than hitRate of the target, it misses
            if (HitRate(target) < hitPercent) return false;
            else return true;
        }

        // Displays Stats
        public void DisplayStats()
        {
            Console.WriteLine("Name: " + stats.Name);
            Console.WriteLine("HP: " + CurrentHp + "/" + stats.MaxHp);
            Console.WriteLine("MP: " + CurrentMp + "/" + stats.MaxMp);
            Console.WriteLine("Power: " + stats.Pow);
            Console.WriteLine("Vitality: " + stats.Vit);
            Console.WriteLine("Agility: " + stats.Agi);
            Console.WriteLine("Dexterity: " + stats.Dex);
        }     

        public List<Skill> Skill { get; set; }
        public Unit()
        {
            Skill = new List<Skill>();
        } 

        public void ReduceMp(int amount)
        {
            currentMp -= amount;
            if (currentMp <= 0) currentMp = 0;
        }
        
        public int BaseDamage (float damageCoefficient)
        {
            int randomDamage = (int)(stats.Pow * CLASS_ADVANTAGE_DAMAGE) + stats.Pow;
            int baseDamage = (int)(randomDamage * damageCoefficient) + stats.Pow;
            return baseDamage;
        }

        public int Damage(Unit Target, float damageCoefficient)
        {
            int damage = (int)((BaseDamage(damageCoefficient) - Target.stats.Vit) * Advantage(Target));
            if (damage <= 0) damage = 1;
            return damage;
        } 

        public void TakeDamage(int damage)
        {
            currentHp -= damage;
            if (currentHp <= 0) currentHp = 0;
        }        
        
        float Advantage(Unit target)
        {
            if (stats.Class == "Mage" && stats.Class == "Warrior" || 
                stats.Class == "Assassin" && stats.Class == "Mage" || 
                stats.Class == "Warrior" && stats.Class == "Assassin") 
            return CLASS_ADVANTAGE_DAMAGE;     
            else return 1.0f;                
        }  

        public int HitRate(Unit target)
        {
            int hitRate = (stats.Dex / target.stats.Agi) * 100;
            //hit% can never be lower than 20 and higher than 80
            if (hitRate <= 20) hitRate = 20;
            else if (hitRate >= 80) hitRate = 80;
            return hitRate;
        }  
    }
}