using System;
using System.Collections.Generic;

namespace _3v3
{
    public class Skill
    {
        // Cost of the skill
        public int MpCost { get; set; }
        // Name of the skill
        public string Name { get; set; }
        // Set the name of the skill
        public void name(string name) { Name = name; }

        // Activates Skill if there's enough MP
        public bool willActivate(Unit actor)
        {
            if (actor.CurrentMp < MpCost) return false;
            else return true;
        }

        // Activates Skill
        public virtual void activateSkill(Unit actor, List<Unit> target, List<Unit> allies) {}
    }
}