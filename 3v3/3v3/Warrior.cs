using System;

namespace _3v3
{
    class Warrior : Unit
    {
        static Random random = new Random();
        public void Initialize(string name, string side)
        {          
            stats.Name = name;
            stats.MaxHp = random.Next(320, 401);
            stats.MaxMp = random.Next(110, 181);

            stats.Pow = random.Next(18, 30);
            stats.Vit = random.Next(10, 21);
            stats.Agi = random.Next(10, 21);
            stats.Dex = random.Next(15, 26);

            CurrentMp = stats.MaxMp;
            CurrentHp = stats.MaxHp;

            stats.Class = "Warrior";
            stats.Side = side;
        }
    }
}
