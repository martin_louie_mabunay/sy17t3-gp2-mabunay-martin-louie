using System;
using System.Collections.Generic;

namespace _3v3
{
    public class BasicAttack : Skill
    {
        static Random random = new Random();
        const float Critical = 1.2f; 
        const float damageCoefficient = 1.0f;

        // Sets the Name of the skill and MP Cost
        public BasicAttack()
        {
            Name = "Basic Attack";
            MpCost = 0;
        }

        // Activates Skill
        public override void activateSkill(Unit actor, List<Unit> target, List<Unit> allies)
        {
            int chosenTarget = 10;

            // Manual targeting for player side
            if (actor.stats.Side == "Team 1")
            {
                // Loops till a valid target has been chosen
                while (chosenTarget > target.Count - 1)
                {
                    // Player will choose a target
                    Console.Write("Choose a target: ");
                    chosenTarget = Convert.ToInt32(Console.ReadLine()) - 1;
                }
            }
            // Enemy picks a target at random
            else chosenTarget = random.Next(target.Count);

            // Critical Chance
            int critChance = random.Next(1, 6);
            // Calculates Damage
            int damage = actor.Damage(target[chosenTarget], damageCoefficient);

            // Checks if alive
            if (actor.Alive == true)
            {
                // Finds a new enemy to target if target is dead
                while (target[chosenTarget].Alive == false) chosenTarget = random.Next(target.Count);
                // Calculates Hit chance
                if (actor.WillHit(target[chosenTarget]) == true)
                {
                    // Critical Chance hits
                    if (critChance == 5)
                    {
                        Console.WriteLine("Critical Hit!");
                        damage = (int)(damage * Critical);
                        Console.WriteLine(actor.stats.Name + " used " + Name + " against " + target[chosenTarget].stats.Name + ".");
                        Console.WriteLine(actor.stats.Name + " dealt " + damage + " damage. ");
                    }
                    // Normal basic attack hits
                    else
                    {
                        Console.WriteLine(actor.stats.Name + " used " + Name + " against " + target[chosenTarget].stats.Name + ".");
                        Console.WriteLine(actor.stats.Name + " dealt " + damage + " damage. ");
                    }
                    // Targeted enemy will receive damage
                    target[chosenTarget].TakeDamage(damage);

                    // Prints the targeted enemy died if it died
                    if (target[chosenTarget].CurrentHp == 0) Console.WriteLine(target[chosenTarget].stats.Name + " died. ");
                    // Delete the dead unit from the list
                    if (target[chosenTarget].Alive == false) target.Remove(target[chosenTarget]);
                }
                // Misses the target
                else Console.WriteLine(actor.stats.Name + "'s " + Name + " missed against " + target[chosenTarget].stats.Name);
            }
            Console.WriteLine();
            Console.ReadLine();
            Console.Clear();       
       }
    }
}
