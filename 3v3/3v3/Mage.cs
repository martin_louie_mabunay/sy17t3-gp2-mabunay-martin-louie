using System;

namespace _3v3
{
    class Mage : Unit
    {
        static Random random = new Random();
        public void Initialize(string name, string side)
        {          
            stats.Name = name;
            stats.MaxHp = random.Next(320, 361);
            stats.MaxMp = random.Next(110, 191);

            stats.Pow = random.Next(18, 25);
            stats.Vit = random.Next(8, 18);
            stats.Agi = random.Next(12, 29);
            stats.Dex = random.Next(15, 26);

            CurrentMp = stats.MaxMp;
            CurrentHp = stats.MaxHp;

            stats.Class = "Mage";
            stats.Side = side;
        }
    }
}
