using System;
using System.Collections.Generic;

namespace MP3 
{
    public class Skill
    {
        // Cost of the skill
        public int MpCost { get; set; }
        // Name of the skill
        public string Name { get; set; }
        // Set the name of the skill
        public void name (string name) { Name = name; }

        public double lowest = Double.MaxValue;
        public int index = -1;

        public void PrintSingleTargetDamageResult(Unit actor, List<Unit> target, int chosenTarget, int damage)
        {
            Console.Clear();
            Console.WriteLine("=================== RESULT ===================");

            Console.WriteLine(actor.Stats.Name + " used " + Name + " against " + target[chosenTarget].Stats.Name + ".");
            Console.WriteLine(actor.Stats.Name + " dealt " + damage + " damage. ");

            // Prints the targeted enemy died if it died
            if (target[chosenTarget].CurrentHp == 0) Console.WriteLine(target[chosenTarget].Stats.Name + " died. ");

            Console.WriteLine("=================== RESULT ===================");
            Console.Write("Action: [Press Enter]");

            Console.ReadLine();
            Console.Clear();
        }

        public void PrintSingleTargetCriticalDamageResult(Unit actor, List<Unit> target, int chosenTarget, int damage)
        {
            Console.Clear();
            Console.WriteLine("=================== RESULT ===================");
            Console.WriteLine("Critical Hit!");
            Console.WriteLine(actor.Stats.Name + " used " + Name + " against " + target[chosenTarget].Stats.Name + ".");
            Console.WriteLine(actor.Stats.Name + " dealt " + damage + " damage. ");

            // Prints the targeted enemy died if it died
            if (target[chosenTarget].CurrentHp == 0) Console.WriteLine(target[chosenTarget].Stats.Name + " died. ");

            Console.WriteLine("=================== RESULT ===================");
            Console.Write("Action: [Press Enter]");

            Console.ReadLine();
            Console.Clear();
        }

        public void PrintSingleTargetHealResult(Unit actor, List<Unit> allies, int chosenTarget, int healAmount)
        {
            Console.Clear();
            Console.WriteLine("================= RESULT =================");

            // Actor targets itself
            if (allies[chosenTarget].Stats.Name == actor.Stats.Name) Console.WriteLine(actor.Stats.Name + " used " + Name + " on theirself ");
            else Console.WriteLine(actor.Stats.Name + " used " + Name + " on " + allies[chosenTarget].Stats.Name);

            // Prints healed amount to target
            Console.WriteLine(allies[chosenTarget].Stats.Name + " was healed for " + healAmount + " HP");

            Console.WriteLine("================= RESULT =================");
            Console.Write("Action: [Press Enter]");

            Console.ReadLine();
            Console.Clear();
        }

        public bool allSameHp(List<Unit> target)
        {
            if (target[0].NormalizedHp == target[0].NormalizedHp && target[0].NormalizedHp == target[1].NormalizedHp && target[0].NormalizedHp == target[2].NormalizedHp &&
                target[1].NormalizedHp == target[0].NormalizedHp && target[1].NormalizedHp == target[1].NormalizedHp && target[1].NormalizedHp == target[2].NormalizedHp &&
                target[2].NormalizedHp == target[0].NormalizedHp && target[2].NormalizedHp == target[1].NormalizedHp && target[2].NormalizedHp == target[2].NormalizedHp) return true;
            else return false;
        }

        public void findLowestHp(List<Unit> target, int index, double lowest, ref int chosenTarget)
        {
            foreach (Unit unit in target)
            {
                index++;
                if (unit.NormalizedHp <= lowest)
                {
                    lowest = unit.NormalizedHp;
                    chosenTarget = index;
                }
            }
        }

        // Activates Skill if there's enough MP
        public bool willActivate (Unit actor)
        {
            if (actor.CurrentMp < MpCost)
            {
                //Console.WriteLine("Insufficient MP!");
                //Console.ReadLine();
                return false;
            }
            else return true;
        }

        public void insufficientMp(Unit actor)
        {
            Console.WriteLine(actor.Stats.Name + " doesn't have enough MP!");
            Console.ReadLine();
            //Console.Clear();
        }


        // Activates Skill
        public virtual void activateSkill (Unit actor, List<Unit> target, List<Unit> allies) { }
    }
}