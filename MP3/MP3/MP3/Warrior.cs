using System;

namespace MP3 
{
    class Warrior : Unit 
    {
        public void Initialize (string name, string side)
        {
            Stats.Name = name;
            Stats.MaxHp = MP3.Global.Random.Next (320, 401);
            Stats.MaxMp = MP3.Global.Random.Next (110, 181);

            Stats.Power = MP3.Global.Random.Next (58, 65);
            Stats.Vitality = MP3.Global.Random.Next (19, 25);
            Stats.Agility = MP3.Global.Random.Next (10, 15);
            Stats.Dexterity = MP3.Global.Random.Next (17, 20);

            CurrentMp = Stats.MaxMp;
            CurrentHp = Stats.MaxHp;

            Job = Job.Warrior;
            Stats.Side = side;
        }
    }
}