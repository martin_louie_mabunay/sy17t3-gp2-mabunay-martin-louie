using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP3 
{
    public class Heal : Skill 
    {
        const float HEAL_MODIFIER = 0.3f;

        // Sets the Name of the skill and MP Cost
        public Heal () 
        {
            Name = "Heal";
            MpCost = 40;
        }

        // Activates Skill
        public override void activateSkill (Unit actor, List<Unit> target, List<Unit> allies) 
        {
            if (actor.Alive == false) return;
            int chosenTarget = 10;

            // Heals for 30% of target's Max HP
            int healAmount = (int)(allies[0].Stats.MaxHp * HEAL_MODIFIER);

            // Checks if you have sufficient Mp
            if (!willActivate(actor)) insufficientMp(actor);

            // Checks if everyone has equal normalized HP
            if (allSameHp(allies)) chosenTarget = MP3.Global.Random.Next(target.Count);

            // Finds ally with lowest HP
            else findLowestHp(allies, index, lowest, ref chosenTarget);

            // Prints Heal taken from the ally
            PrintSingleTargetHealResult(actor, allies, chosenTarget, healAmount);

            // Heals chosen ally
            allies[chosenTarget].Heal (healAmount);

            // Deducts Mana based on MpCost
            actor.ReduceMp (MpCost);
        }
    }
}