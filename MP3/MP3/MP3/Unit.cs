using System;
using System.Collections.Generic;

namespace MP3
{
    public enum Job { Warrior, Assassin, Mage }
    public class Unit
    {
        public const float CLASS_ADVANTAGE_DAMAGE_MULTIPLIER = 1.5f;
        public const float RANDOM_POW_DAMAGE = 0.2f;
        public const int MAX_HIT_RATE = 80;
        public const int MIN_HIT_RATE = 20;

        Skill basicAttack = new Skill();
        Skill skill = new Skill();


        public Stats Stats;
        public Job Job { get; set; }


        // Current HP of a Unit
        private int currentHp;

        public int CurrentHp
        {
            get { return currentHp; }
            set
            {
                // Clamps HP
                currentHp = value;
                if (currentHp < 0) currentHp = 0;
                if (currentHp > Stats.MaxHp) currentHp = Stats.MaxHp;
            }
        }

        // Current MP of a Unit
        private int currentMp;
        public int CurrentMp
        {
            get { return currentMp; }
            set
            {
                // Clamps MP
                currentMp = value;
                if (currentMp < 0) currentMp = 0;
                if (currentMp > Stats.MaxHp) currentMp = Stats.MaxMp;
            }
        }

        public int NormalizedHp
        {
            get
            {
                int normalized = (int)Math.Ceiling((double)(100 * CurrentHp) / Stats.MaxHp);
                return normalized;
            }
        }

        public int NormalizedMp
        {
            get
            {
                int normalized = (int)Math.Round((double)(100 * CurrentMp) / Stats.MaxMp);
                return normalized;
            }
        }

        public void Heal(int amount)
        {
            currentHp += amount;
            if (currentHp > Stats.MaxHp) currentHp = Stats.MaxHp;
        }

        // Checks if Unit is alive
        public bool Alive
        {
            get
            {
                if (CurrentHp <= 0) return false;
                else return true;
            }
        }

        // Checks if Skill will hit the target
        public bool WillHit(Unit target)
        {
            int hitRate = (Stats.Dexterity / target.Stats.Agility) * 100;
            int hitPercent = MP3.Global.Random.Next(1, 101);

            // Hit% can never be lower than 20 and higher than 80
            if (hitRate <= 20) hitRate = 20;
            else if (hitRate >= 80) hitRate = 80;

            // if hitPercent is greater than hitRate of the target, it misses
            if (hitRate < hitPercent) return false;
            else return true;
        }

        // Displays Stats
        public void DisplayUnit()
        {
            Console.WriteLine("Class: " + Job);
            Console.WriteLine("Name: " + Stats.Name);
            Console.WriteLine("HP: " + CurrentHp + "/" + Stats.MaxHp + " [" + NormalizedHp + "%]");
            Console.WriteLine("MP: " + CurrentMp + "/" + Stats.MaxMp + " [" + NormalizedMp + "%]");
            Console.WriteLine("Power: " + Stats.Power);
            Console.WriteLine("Vitality: " + Stats.Vitality);
            Console.WriteLine("Agility: " + Stats.Agility);
            Console.WriteLine("Dexterity: " + Stats.Dexterity);
        }

        public List<Skill> Skill { get; set; }
        public Unit() { Skill = new List<Skill>(); }

        public bool allSame(List<Unit> target)
        {
            if (target[0].CurrentHp == target[0].CurrentHp || target[0].CurrentHp == target[1].CurrentHp || target[0].CurrentHp == target[2].CurrentHp &&
                target[1].CurrentHp == target[0].CurrentHp || target[1].CurrentHp == target[1].CurrentHp || target[1].CurrentHp == target[2].CurrentHp &&
                target[2].CurrentHp == target[0].CurrentHp || target[2].CurrentHp == target[1].CurrentHp || target[2].CurrentHp == target[2].CurrentHp) return true;
            else return false;
        }

        public bool isMaxHp()
        {
            if (currentHp == Stats.MaxHp) return true;
            else return false;
        }

        public void ReduceMp(int amount)
        {
            currentMp -= amount;
            if (currentMp <= 0) currentMp = 0;
        }

        public int BaseDamage()
        {
            int randomVariance = MP3.Global.Random.Next(20);
            randomVariance = randomVariance / 100;
            int baseDamage = Stats.Power + (int)(randomVariance * Stats.Power);
            return baseDamage;
        }

        public int Damage(Unit Target, float damageCoefficient)
        {
            //Console.WriteLine("Hi");
            int damage = (int)((BaseDamage() - Target.Stats.Vitality) * EvaluateDamageMultiplier(Target) * damageCoefficient);
            if (damage <= 0) damage = 1;
            return damage;
        }

        public void TakeDamage(int damage)
        {
            currentHp -= damage;
            if (currentHp <= 0) currentHp = 0;
        }

        public float EvaluateDamageMultiplier(Unit Target)
        {
            if (Job == Job.Mage && Target.Job == Job.Warrior ||
               Job == Job.Assassin && Target.Job == Job.Mage ||
               Job == Job.Warrior && Target.Job == Job.Assassin)
                return CLASS_ADVANTAGE_DAMAGE_MULTIPLIER;

            else return 1.0f;
        }
    }
}