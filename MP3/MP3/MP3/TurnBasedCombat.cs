﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP3
{
    class TurnBasedCombat
    {
        public void Main()
        {
            List<Unit> playerTeam = new List<Unit>();
            List<Unit> enemyTeam = new List<Unit>();
            List<Unit> Units = new List<Unit>();

            // Initializes Units and Skills
            InitializeTeam(playerTeam, enemyTeam);

            // Sort the Turn Order
            TurnOrder(playerTeam, enemyTeam, Units);

            // Loops till no one is left on a team
            while (playerTeam.Count > 0 && enemyTeam.Count > 0) Battle(playerTeam, enemyTeam, Units);

            // Prints Game Over results
            gameOver(playerTeam, enemyTeam);
        }

        private void playerChoose(List<Unit> Units, ref Skill skill, ref int playerChoice)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            AskPlayer(Units[0], Units);
            playerChoice = Convert.ToInt32(Console.ReadLine());
            if (playerChoice == 1)
            {
                skill = Units[0].Skill[0];
            }
            else if (playerChoice == 2)
            {
                skill = Units[0].Skill[1];
            }
            
            while (playerChoice >= Units[0].Skill.Count && !skill.willActivate(Units[0]))
            {
                if (!skill.willActivate(Units[0]))
                {
                    Console.Clear();
                    AskPlayer(Units[0], Units);
                    MP3.Global.ClearCurrentConsoleLine();
                    Console.WriteLine("Action: Press [Enter]");
                    Console.Write("Insufficient MP!");
                    Console.ReadLine();
                    AskPlayer(Units[0], Units);
                    playerChoice = Convert.ToInt32(Console.ReadLine());
                    if (playerChoice == 1)
                    {
                        skill = Units[0].Skill[0];
                    }
                    else if (playerChoice == 2)
                    {
                        skill = Units[0].Skill[1];
                    }
                }
                else if (playerChoice >= Units[0].Skill.Count)
                {
                    Console.Clear();
                    AskPlayer(Units[0], Units);
                    MP3.Global.ClearCurrentConsoleLine();
                    Console.WriteLine("Action: Press [Enter]");
                    Console.Write("Invalid Input!");
                    Console.ReadLine();
                    AskPlayer(Units[0], Units);
                    playerChoice = Convert.ToInt32(Console.ReadLine());
                    if (playerChoice == 1)
                    {
                        skill = Units[0].Skill[0];
                    }
                    else if (playerChoice == 2)
                    {
                        skill = Units[0].Skill[1];
                    }
                }
            }
        }

        private void Battle(List<Unit> playerTeam, List<Unit> enemyTeam, List<Unit> Units)
        {

            // Shockwave ans Assassinate problems
            Skill skill = null;

            int playerChoice = 0;
            int enemyChoice = MP3.Global.Random.Next(1, 3);
            PrintUnits(playerTeam, enemyTeam);
            PrintOrder(Units);

            skill = Units[0].Skill[1];

            if (Units[0].Stats.Side == "Team 1" )
            {
                // Changes Color to Team Color
                Console.ForegroundColor = ConsoleColor.DarkCyan;

                // Player chooses an action
                playerChoose(Units, ref skill, ref playerChoice);
                PrintTargets(playerTeam, enemyTeam);

                // Targets enemy team and activates the chosen skill
                skill.activateSkill(Units[0], enemyTeam, playerTeam);
            }
            else 
            {
                // Changes Color to Team Color
                Console.ForegroundColor = ConsoleColor.DarkRed;

                if (enemyChoice == 1) skill = Units[0].Skill[0];
                else if (enemyChoice == 2)
                {
                    skill = Units[0].Skill[1];
                    if (!skill.willActivate(Units[0])) skill = Units[0].Skill[0];
                }

                // Targets player  team and activates the random chosen skill
                skill.activateSkill(Units[0], playerTeam, enemyTeam);
            }
            Console.ForegroundColor = ConsoleColor.Gray;
            NextTurn(Units);
        }

        private void InitializeTeam(List<Unit> playerTeam, List<Unit> enemyTeam)
        {
            // Skill Initializers
            #region 
            // Basic Attack
            BasicAttack basicAttack = new BasicAttack();
            // Shockwave
            Shockwave shockWave = new Shockwave();
            // Heal
            Heal heal = new Heal();
            // Assassinate
            Assassinate assassinate = new Assassinate();
            #endregion

            // Player Team Initializer
            #region
            // Warrior
            Warrior playerWarrior = new Warrior();
            playerWarrior.Initialize("Ally Warrior", "Team 1");
            playerTeam.Add(playerWarrior);
            // Warrior Skills
            playerWarrior.Skill.Add(basicAttack);
            playerWarrior.Skill.Add(shockWave);

            // Wizard
            Mage playerMage = new Mage();
            playerMage.Initialize("Ally Wizard", "Team 1");
            playerTeam.Add(playerMage);
            // Wizard Skills
            playerMage.Skill.Add(basicAttack);
            playerMage.Skill.Add(heal);

            // Assassin
            Assassin playerAssassin = new Assassin();
            playerAssassin.Initialize("Ally Assassin", "Team 1");
            playerTeam.Add(playerAssassin);
            // Assassin Skills
            playerAssassin.Skill.Add(basicAttack);
            playerAssassin.Skill.Add(assassinate);
            #endregion

            // Enemy Team Initializer
            #region
            // Warrior
            Warrior enemyWarrior = new Warrior();
            enemyWarrior.Initialize("Enemy Warrior", "Team 2");
            enemyTeam.Add(enemyWarrior);
            // Warrior Skills
            enemyWarrior.Skill.Add(basicAttack);
            enemyWarrior.Skill.Add(shockWave);

            // Wizard
            Mage enemyMage = new Mage();
            enemyMage.Initialize("Enemy Wizard", "Team 2");
            enemyTeam.Add(enemyMage);
            // Wizard Skills
            enemyMage.Skill.Add(basicAttack);
            enemyMage.Skill.Add(heal);

            // Assassin
            Assassin enemyAssassin = new Assassin();
            enemyAssassin.Initialize("Enemy Assassin", "Team 2");
            enemyTeam.Add(enemyAssassin);
            // Assassin Skills
            enemyAssassin.Skill.Add(basicAttack);
            enemyAssassin.Skill.Add(assassinate);
            #endregion
        }

        private void PrintUnits(List<Unit> playerTeam, List<Unit> enemyTeam)
        {
            // Prints Team 1
            Console.WriteLine("Team 1 ");
            Console.WriteLine("====================================");
            foreach (Unit unit in playerTeam) Console.WriteLine(unit.Stats.Name + " HP: " + unit.CurrentHp + "/" + unit.Stats.MaxHp + " [" + unit.NormalizedHp + "%]");
            Console.WriteLine("====================================");
            Console.WriteLine();

            // Prints Team 2
            Console.WriteLine("Team 2 ");
            Console.WriteLine("====================================");
            foreach (Unit unit in enemyTeam) Console.WriteLine(unit.Stats.Name + " HP: " + unit.CurrentHp + "/" + unit.Stats.MaxHp + " [" + unit.NormalizedHp + "%]");
            Console.WriteLine("====================================");
            Console.WriteLine();
        }

        private void PrintAllies(List<Unit> playerTeam)
        {
            int i = 1;
            Console.WriteLine("=========================================");
            foreach (Unit unit in playerTeam)
            {
                Console.WriteLine("[" + i + "]" + unit.Stats.Name + " [HP: " + unit.CurrentHp + "]");
                i++;
            }
            Console.WriteLine("=========================================");
        }

        private void PrintTargets(List<Unit> playerTeam, List<Unit> enemyTeam)
        {
            Console.Clear();
            int i = 1;
            Console.WriteLine("=========================================");
            foreach (Unit unit in enemyTeam)
            {
                Console.WriteLine("[" + i + "]" + unit.Stats.Name + " HP: " + unit.CurrentHp + "/" + unit.Stats.MaxHp + " [" + unit.NormalizedHp + "%]");
                i++;
            }
            Console.WriteLine("=========================================");
            Console.WriteLine();
        }

        private void TurnOrder(List<Unit> playerTeam, List<Unit> enemyTeam, List<Unit> Units)
        {
            Unit temp;
            foreach (Unit unit in playerTeam) Units.Add(unit);
            foreach (Unit unit in enemyTeam) Units.Add(unit);
            for (int i = 0; i < Units.Count; i++)
            {
                for (int j = i + 1; j < Units.Count; j++)
                {
                    deleteDead(Units);
                    if (Units[j].Stats.Agility > Units[i].Stats.Agility)
                    {
                        temp = Units[i];
                        Units[i] = Units[j];
                        Units[j] = temp;
                    }
                }
            }
        }

        private void PrintOrder(List<Unit> Units)
        {
            int currentTurn = 1;

            deleteDead(Units);
            Console.WriteLine("========= TURN ORDER =========");
            foreach (Unit unit in Units)
            {
                //Prints the side and name
                if (unit.Alive == true) Console.WriteLine("#" + currentTurn + " [" + unit.Stats.Side + "] " + unit.Stats.Name);
                currentTurn++;
            }
            Console.WriteLine("========= TURN ORDER =========");
            Console.WriteLine("Current Turn: " + Units[0].Stats.Name);
            Console.Write("Action: [Press Enter]");
            Console.ReadLine();
            Console.Clear();
        }

        private void AskPlayer(Unit player, List<Unit> Units)
        {
            Console.Clear();
            player.DisplayUnit();
            Console.WriteLine();
            Console.WriteLine("Choose an Action... ");
            Console.WriteLine("=========================");
            Console.WriteLine("[1] " + Units[0].Skill[0].Name + " [MP Cost: " + Units[0].Skill[0].MpCost + "]");
            Console.WriteLine("[2] " + Units[0].Skill[1].Name + " [MP Cost: " + Units[0].Skill[1].MpCost + "]");
            Console.WriteLine("=========================");
            Console.Write("Action: ");
        }

        private void NextTurn(List<Unit> Units)
        {
            int i = 0;
            Unit temp;

            for (int j = i + 1; j < Units.Count; j++)
            {
                temp = Units[i];
                Units[i] = Units[j];
                Units[j] = temp;
                i++;
            }
        }

        private void deleteDead(List<Unit> Units)
        {
            for (int i = Units.Count - 1; i >= 0; i--) if (Units[i].Alive == false) Units.RemoveAt(i);
        }

        private void gameOver(List<Unit> playerTeam, List<Unit> enemyTeam)
        {
            Console.WriteLine("========= GAME OVER =========");
            if (playerTeam.Count > 0) Console.WriteLine("Team 1 Wins!");
            else if (enemyTeam.Count > 0) Console.WriteLine("Team 2 Wins!");
            Console.WriteLine("========= GAME OVER =========");
            Console.ReadLine();
        }
    }
}