using System;

namespace MP3 
{
    class Mage : Unit 
    {
        public void Initialize (string name, string side) 
        {
            Stats.Name = name;
            Stats.MaxHp = MP3.Global.Random.Next (320, 361);
            Stats.MaxMp = MP3.Global.Random.Next (110, 191);

            Stats.Power = MP3.Global.Random.Next (50, 56);
            Stats.Vitality = MP3.Global.Random.Next (15, 29);
            Stats.Agility = MP3.Global.Random.Next (12, 21);
            Stats.Dexterity = MP3.Global.Random.Next (15, 23);

            CurrentMp = Stats.MaxMp;
            CurrentHp = Stats.MaxHp;

            Job = Job.Mage;
            Stats.Side = side;
        }
    }
}