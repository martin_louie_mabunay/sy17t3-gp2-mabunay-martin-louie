using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP3
{
    public struct Stats
    {
        public string Name { get; set; }
        public int MaxHp { get; set; }
        public int Hp { get; set; }
        public int MaxMp { get; set; }
        public int Mp { get; set; }
        public int Power { get; set; }
        public int Vitality { get; set; }
        public int Agility { get; set; }
        public int Dexterity { get; set; }
        public string Side { get; set; }
    }
}
