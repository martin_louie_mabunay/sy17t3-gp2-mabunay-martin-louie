using System;
using System.Collections.Generic;

namespace MP3 
{
    public class BasicAttack : Skill 
    {
        const float Critical = 1.2f;
        const float damageCoefficient = 1.0f;

        // Sets the Name of the skill and MP Cost
        public BasicAttack () 
        {
            Name = "Basic Attack";
            MpCost = 0;
        }

        private int chooseTarget (int chosenTarget, List<Unit> target)
        {
            // Loops till a valid target has been chosen
            while (chosenTarget > target.Count - 1)
            {
                // Player will choose a target  
                Console.Write("Choose a target: ");
                chosenTarget = Convert.ToInt32(Console.ReadLine()) - 1;
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                MP3.Global.ClearCurrentConsoleLine();
            }
            return chosenTarget;
        }

        private int criticalChance (int chosenTarget, int critChance, int damage, float Critical, List<Unit> target, Unit actor)
        {
            // Critical Chance hits
            if (critChance == 5)
            {
                damage = (int)(damage * Critical);
                PrintSingleTargetCriticalDamageResult(actor, target, chosenTarget, damage);
            }

            // Normal basic attack hits
            else PrintSingleTargetDamageResult(actor, target, chosenTarget, damage);
            return damage;
        }

        private void PrintMissedTarget(Unit actor, List<Unit> target, int chosenTarget)
        {
            Console.WriteLine("================= RESULT =================");
            Console.WriteLine(actor.Stats.Name + "'s " + Name + " missed against " + target[chosenTarget].Stats.Name);
            Console.WriteLine("================= RESULT =================");
            Console.Write("Action: [Press Enter]");
            Console.ReadLine();
            Console.Clear();
        }

        // Activates Skill
        public override void activateSkill (Unit actor, List<Unit> target, List<Unit> allies) 
        {
            int chosenTarget = target.Count + 1;
            // Checks if alive
            if (!actor.Alive) return;

            // Manual targeting for player side
            if (actor.Stats.Side == "Team 1") chosenTarget = chooseTarget(chosenTarget, target);
            // Enemy picks a target at random
            else chosenTarget = MP3.Global.Random.Next(target.Count);

            // Critical Chance
            int critChance = MP3.Global.Random.Next (1, 6);

            // Calculates Damage
            int damage = actor.Damage (target[chosenTarget], damageCoefficient);

            // Finds a new enemy to target if target is dead
            while (target[chosenTarget].Alive == false) chosenTarget = MP3.Global.Random.Next (target.Count);

            // Calculates Hit chance
            if (actor.WillHit(target[chosenTarget]))
            {
                // Critical Chance hits
                damage = criticalChance(chosenTarget, critChance, damage, Critical, target, actor);

                // Targeted enemy will receive damage
                target[chosenTarget].TakeDamage(damage);

                // Prints targets who died
                foreach (Unit unit in target) if (!unit.Alive) Console.WriteLine(unit.Stats.Name + " died ");

                // Delete the dead unit from the list
                if (target[chosenTarget].Alive == false) target.Remove(target[chosenTarget]);
            }

            // Misses the target
            else PrintMissedTarget(actor, target, chosenTarget);
        }
    }
}