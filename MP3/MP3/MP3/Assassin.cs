using System;

namespace MP3 
{
    class Assassin : Unit 
    {
        public void Initialize (string name, string side) 
        {
            Stats.Name = name;
            Stats.MaxHp = MP3.Global.Random.Next (300, 351);
            Stats.MaxMp = MP3.Global.Random.Next (135, 191);

            Stats.Power = MP3.Global.Random.Next (73, 80);
            Stats.Vitality = MP3.Global.Random.Next (9, 18);
            Stats.Agility = MP3.Global.Random.Next (19, 25);
            Stats.Dexterity = MP3.Global.Random.Next (19, 21);

            CurrentMp = Stats.MaxMp;
            CurrentHp = Stats.MaxHp;

            Job = Job.Assassin;
            Stats.Side = side;
        }
    }
}