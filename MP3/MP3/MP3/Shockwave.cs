using System;
using System.Collections.Generic;

namespace MP3 
{
    public class Shockwave : Skill 
    {
        const float SHOCKWAVE_DAMAGE = 0.9f;

        // Sets the Name of the skill and MP Cost
        public Shockwave () 
        {
            Name = "Shockwave";
            MpCost = 50;
        }

        // Activates Skill
        public override void activateSkill (Unit actor, List<Unit> target, List<Unit> allies) 
        {
            if (actor.Alive == false) return;

            // Checks if you have sufficient Mp
            if (!willActivate(actor)) insufficientMp(actor);

            Console.Clear();
            Console.WriteLine("================= RESULT =================");
            Console.WriteLine (actor.Stats.Name + " is casting " + Name + " against all opponents ");

            // Different damage values targets
            foreach (Unit unit in target) 
            {                    
                int damage = actor.Damage(unit, SHOCKWAVE_DAMAGE);
                Console.WriteLine (Name + " dealt " + damage + " against " + unit.Stats.Name);
                unit.TakeDamage (damage);
            }

            // Prints targets who died
            foreach (Unit unit in target) if (!unit.Alive) Console.WriteLine (unit.Stats.Name + " died ");

            Console.WriteLine("================= RESULT =================");
            Console.Write("Action: [Press Enter]");

            // Delete dead targets from the list
            for (int i = target.Count - 1; i >= 0; i--) if (target[i].Alive == false) target.RemoveAt (i);

            // Deducts Mana based on MpCost
            actor.ReduceMp (MpCost);
            
            Console.ReadLine ();
            Console.Clear ();
        }
    }
}