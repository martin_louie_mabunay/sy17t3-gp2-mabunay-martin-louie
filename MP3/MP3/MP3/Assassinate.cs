using System;
using System.Collections.Generic;
using System.Linq;

namespace MP3 
{
    public class Assassinate : Skill 
    {
        const float ASSASSINATE_DAMAGE = 2.2f;

        // Sets the Name of the skill and MP Cost
        public Assassinate () 
        {
            Name = "Assassinate";
            MpCost = 45;
        }

        // Activates Skill
        public override void activateSkill (Unit actor, List<Unit> target, List<Unit> allies)
        {
            int chosenTarget = target.Count + 1;
            if (actor.Alive == false) return;

            int damage = actor.Damage (target[0], ASSASSINATE_DAMAGE);

            // Checks if everyone has equal normalized HP
            if (allSameHp(target)) chosenTarget = MP3.Global.Random.Next(target.Count);

            // Finds target with lowest HP
            else findLowestHp(target, index, lowest, ref chosenTarget);

            // Prints Damage taken from the target
            PrintSingleTargetDamageResult(actor, target, chosenTarget, damage);

            // Targeted enemy will receive damage
            target[chosenTarget].TakeDamage (damage);

            // Prints targets who died
            foreach (Unit unit in target) if (!unit.Alive) Console.WriteLine(unit.Stats.Name + " died ");

            // Delete the dead unit from the list
            if (target[chosenTarget].Alive == false) target.Remove (target[chosenTarget]);

            // Deducts Mana based on MpCost
            actor.ReduceMp (MpCost);
        }
    }
}